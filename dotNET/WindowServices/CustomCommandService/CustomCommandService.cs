﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;

namespace Service
{
	/// <summary>
	/// This is a sample service that does nothing more than
	/// logging messages.  It will log a message on start, stop,
	/// and upon receiving a custom 128 command.
	/// </summary>
	partial class CustomCommandService : ServiceBase
	{
		/// <summary>
		/// Ctor
		/// </summary>
		public CustomCommandService()
		{
			InitializeComponent();
		}
		/// <summary>
		/// Custom commands for the service.
		/// </summary>
		public enum CustomCommands
		{
			WakeUp = 128,
		}
		/// <summary>
		/// Logs a message to a log file at c:\wakeupservice.log
		/// </summary>
		/// <param name="text">the text to log</param>
		private void Log(string text)
		{
			File.AppendAllLines("C:\\customcommandservice.log", new[] { string.Format("[{0:yyyy-MM-dd HH:mm:ss}] - {1}", DateTime.Now, text) });
		}
		/// <summary>
		/// Processes the custom commands.
		/// </summary>
		/// <param name="command">the ID of the command to process</param>
		protected override void OnCustomCommand(int command)
		{
			switch ((CustomCommands)command)
			{
				case CustomCommands.WakeUp:
					Log("Wake up!");
					break;
			}
		}

		protected override void OnStart(string[] args)
		{
			Log("Started!");
		}

		protected override void OnStop()
		{
			Log("Stopped!");
		}
	}
}
