﻿namespace Service
{
    partial class CustomCommandServiceInstaller
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
			this.ServiceInstall = new System.ServiceProcess.ServiceInstaller();
			this.ServiceProcessInstall = new System.ServiceProcess.ServiceProcessInstaller();
			// 
			// ServiceInstall
			// 
			this.ServiceInstall.Description = "A sample project for a windows service implementing custom commands.";
			this.ServiceInstall.DisplayName = "Sample Custom Command Service";
			this.ServiceInstall.ServiceName = "CustomCommandService";
			// 
			// ServiceProcessInstall
			// 
			this.ServiceProcessInstall.Account = System.ServiceProcess.ServiceAccount.LocalSystem;
			this.ServiceProcessInstall.Password = null;
			this.ServiceProcessInstall.Username = null;
			// 
			// CustomCommandServiceInstaller
			// 
			this.Installers.AddRange(new System.Configuration.Install.Installer[] {
            this.ServiceInstall,
            this.ServiceProcessInstall});

        }

        #endregion

        private System.ServiceProcess.ServiceInstaller ServiceInstall;
        private System.ServiceProcess.ServiceProcessInstaller ServiceProcessInstall;
    }
}