﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;

namespace MultiService
{
	public partial class MultiServiceI : ServiceBase
	{
		public MultiServiceI()
		{
			InitializeComponent();
		}

		protected override void OnStart(string[] args)
		{
			// Log that the service is running, and show what was the last to start
			// just to show that the "LastService" is shared between instances of
			// the service.
			Program.Log("Started MultiService I, Last Service Started = " + (Program.LastService ?? string.Empty));
			Program.LastService = "MultiService I";
		}

		protected override void OnStop()
		{
		}
	}
}
