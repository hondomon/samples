﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;

namespace MultiService
{
	static class Program
	{
		/// <summary>
		/// The main entry point for the application.
		/// </summary>
		static void Main()
		{
			ServiceBase[] ServicesToRun;
			ServicesToRun = new ServiceBase[] 
            { 
                new MultiServiceI(), 
				new MultiServiceII()
            };
			ServiceBase.Run(ServicesToRun);
		}

		public static string LastService = "";

		public static void Log(string logText)
		{
			File.AppendAllLines("C:\\multiservice.log", new[] { logText });
		}
	}
}
