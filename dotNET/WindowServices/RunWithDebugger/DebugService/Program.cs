﻿/* 
 * Written as a code sample for devjeremy.wordpress.com free for re-use.
 */

using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace DebugService
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        static void Main(string[] args)
        {
            // If the debugger is attached, the program runs inline rather
            // than it being run as a service.
            MyService serviceToRun = new MyService();

            #if DEBUG
            if (Debugger.IsAttached)
            {
                serviceToRun.Start(args);

                while (Debugger.IsAttached)
                {
                    Thread.Sleep(1000);
                }
            }
            else
            #endif
            {
                ServiceBase.Run(new[] { serviceToRun });
            }
        }
    }
}
