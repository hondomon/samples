/* Written by Jeremy Honl, 2012 as an exercise for oracle programming. This script is free 
 * for public use.
 * 
 * To use this script, the Oracle .NET client access library must be installed.  While the 
 * connection does use an express edition of Oracle, any version of Oracle should work fine.
 */

$global:DefaultConnectionString="Data Source=localhost/XE;User ID=system;Password=password;"

[System.Reflection.Assembly]::LoadWithPartialName("Oracle.DataAccess");



<#
.SYNOPSIS
	Invokes a SQL statement against an oracle database.
.PARAM Sql
    The SQL to execute.
.PARAM ConnectionString
    The connection string to use to connect to the database.  If it is not available, $global:DefaultConnectionString is used as the default.
.PARAM Timeout
    The time to wait before timing out the command.
.EXAMPLE
    Invoke-OracleSql -Sql "SELECT * FROM ALL_USERS" -ConnectionString "Data Source=localhost/XE;User ID=system;Password=password;" -Timeout 60
#>
function Invoke-OracleSql
{
    param($Sql, $ConnectionString = $global:DefaultConnectionString, $Timeout=30)
    
	$table = New-Object System.Data.DataTable

    $connect = New-Object Oracle.DataAccess.Client.OracleConnection
    try
    {
        $connect.ConnectionString = $ConnectionString
        $cmd = $connect.CreateCommand()
        $cmd.CommandText = $Sql
        $cmd.CommandTimeout = $Timeout
    

	    $adapter = New-Object Oracle.DataAccess.Client.OracleDataAdapter -ArgumentList @($cmd)
	    $adapter.Fill($table);
	
	    return $table.Rows;
    }
    finally
    {
        if ($connect.State -eq [System.Data.ConnectionState]::Open)
        {
            $connect.Close()
        }
    }
}