﻿##############################################
# Set up WinRM for remote connections
#

# Configure WinRM for access from the machine
winrm quickconfig

# Allow access from all hosts from the current workgroup.
# Other machines only need to be added to trustedhosts if there
# is some sort of authentication problem.  Typically if the
# workstations are set up in a trusted domain, this step 
# is not needed.
winrm set winrm/config/client @{TrustedHosts="<local>"}



# Alternatively, it can be done through powershell via
Enable-PSRemoting

# If additional hosts need to be added to trusted hosts, it can be done
# with the following powershell command.
Set-Item -Path WSMan:\localhost\client\TrustedHosts -Value "<local>"
