SET SERVEROUTPUT ON;

/*******************************************************************************
 | PL/SQL has several different types of loops. The first being a loop where
 | an EXIT statement inline determines when to exit the loop.
 |
 | If an exit statement is not used within the loop, it will be an infinite loop
 | which will not exit.
 |
 | The other variants of the LOOP can be re-written using this statement.
 ******************************************************************************/
DECLARE
  current_value INTEGER := 0;
BEGIN
  LOOP
    EXIT WHEN current_value > 5;
    
    DBMS_OUTPUT.PUT_LINE('current_value: ' || current_value);
    current_value := current_value + 1;
  END LOOP;
END;
/

/*******************************************************************************
 | The next is an iterative loop using a range from the starting number to the
 | the end of the range.  The number is always increased by one and there is no
 | ability to provide a step for the range.
 |
 | The conditions in the range are determined before the loop executes. 
 | Any changes during the loop will not change the range conditions.
 ******************************************************************************/
BEGIN
  FOR CURRENT_VALUE IN 0 .. 5 LOOP
    DBMS_OUTPUT.PUT_LINE('current_value: ' || current_Value);
  END LOOP;
END;
/

/*******************************************************************************
 | The iterative loop can also have the range reversed to count backwards.
 ******************************************************************************/
BEGIN
  FOR CURRENT_VALUE IN REVERSE 0 .. 5 LOOP
    DBMS_OUTPUT.PUT_LINE('current_value: ' || CURRENT_VALUE);
  END LOOP;
END;
/


/*******************************************************************************
 | The for loop can also use a implicit cursor to iterate through the entire
 | result set of a query.
 ******************************************************************************/
BEGIN
  FOR cur_table IN (SELECT table_name FROM ALL_TABLES) LOOP
    DBMS_OUTPUT.PUT_LINE('table: ' || cur_table.table_name);
  END LOOP;
END;
/


/*******************************************************************************
 | The final loop is a while loop in which the condition is evaluated prior to
 | the loop running and before every iteration of the loop there after. Once the
 | condition no longer matches, the loop will be complete.
 ******************************************************************************/
DECLARE
  CURRENT_VALUE INTEGER := 0;
BEGIN
  WHILE CURRENT_VALUE < 6 LOOP
    DBMS_OUTPUT.PUT_LINE('current_value: ' || CURRENT_VALUE);
    current_value := current_value + 1;
  END LOOP;
END;
/


