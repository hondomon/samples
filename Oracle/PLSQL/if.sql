SET SERVEROUTPUT ON;


/******************************************************************************* 
 | Null conditions are evaluated as false in an if statement.  For example, 
 | comparing an integer whose value is null will evaluate to NULL rather than 
 | causing a "null reference exception" like .NET or JAVA.  This is also 
 | referred to as an "undefined" value in relational database theory.
 ******************************************************************************/
DECLARE
  null_condition INTEGER;
BEGIN
  null_condition := NULL;
  
  IF null_condition < 10 THEN
    DBMS_OUTPUT.PUT_LINE('This is not true, null_condition is null.');
  ELSE
    DBMS_OUTPUT.PUT_LINE(
      'A null condition will be evaluated as false, and wind up in the ELSE in '
      || 'this scenario.'
    );
  END IF;
  
END;
/


/*******************************************************************************
 | Here's a full IF block. Note that the ELSIF has no "E", it is not "ELSEIF".
 | Other than that, the if statement is much the same as that of other 
 | programming languages.
 |
 | Unlike a CASE statement, if no conditions are satisfied, execution of the
 | program continues.
 ******************************************************************************/
DECLARE
  cost_of_a_laptop INTEGER := 500;
BEGIN
    
  IF cost_of_a_laptop <= 0 THEN
    DBMS_OUTPUT.PUT_LINE('This must be a gimmic');
  ELSIF cost_of_a_laptop BETWEEN 1 AND 299 THEN
    DBMS_OUTPUT.PUT_LINE('This must be a netbook');
  ELSIF cost_of_a_laptop BETWEEN 300 AND 599 THEN
    DBMS_OUTPUT.PUT_LINE('This may last you a year.');
  ELSIF cost_of_a_laptop >= 600 THEN
    DBMS_OUTPUT.PUT_LINE('This may last you a few years');
  ELSE  
    DBMS_OUTPUT.PUT_LINE('cost_of_a_laptop IS NULL');
  END IF;

END;
/