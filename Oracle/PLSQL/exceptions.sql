SET SERVEROUTPUT ON;

/*******************************************************************************
 | There are several ways to deal with errors in PL/SQL programs.  Named 
 | exceptions can be defined and raised within a block of code.  Within a
 | block of code, an EXCEPTION section can be implemented which then catches
 | raised exceptions.
 |
 | The problem with this is that the exception is unknown outside of the block 
 | that defined it.  As such, the exception would be propogated as a user 
 | defined error with no additional details.
 |
 | It is possible to catch exceptions via the EXCEPTION clause within a block.
 | The exception clause can then be used to capture specific exceptions and do
 | processing based on the exception itself.
 |
 | There are a number of exceptions in Oracle that are named that can be caught
 | in this fashion.  For those which aren't named a PRAGMA can be used to
 | create a friendly name for them.
 |
 | SQLCODE = The error number
 | SQLERRM = The error message
 ******************************************************************************/

DECLARE
  NOT_AVAILABLE EXCEPTION;
BEGIN

  RAISE NOT_AVAILABLE;
  
EXCEPTION
  WHEN NOT_AVAILABLE THEN
    DBMS_OUTPUT.PUT_LINE('NOT_AVAILABLE  Error Code: ' || SQLCODE || ' Message: ' || SQLERRM);
END;
/



/*******************************************************************************
 * One way to handle the scenario where an exception must be able to be handled
 * outside of the application is to define an error code with the user defined
 * exception.  This is done through PRAGMA EXCEPTION_INIT
 *
 * However, using this method, the error message that is passed along with the 
 * exception is empty (other than the error message).
 *
 * Note the use of the WHEN OTHERs clause in this example as well.  This 
 * is used as a catch all for any previously undefined exceptions.
 *
 * The range for user defined error codes is from -20999 to -20000.  This is
 * quite dissapointing.  1000 error codes seems much too small for todays uses.
 * As a way to counteract this, sometimes the message will be used in order to
 * convey a code.  Some applications will use GUIDs, others will just continue
 * with a numbering scheme that is included as part of the message in the error.
 ******************************************************************************/
BEGIN
   
  DECLARE 
    APPLICATION_ERROR EXCEPTION;
    PRAGMA EXCEPTION_INIT(APPLICATION_ERROR, -20000);
  BEGIN
  
    RAISE APPLICATION_ERROR;
    
  END;

EXCEPTION
  WHEN OTHERS THEN
    DBMS_OUTPUT.PUT_LINE('Error Code: ' || SQLCODE || ' Message: ' || SQLERRM);
END;
/

/*******************************************************************************
 * Another alternative to supply an error back is to use RAISE_APPLICATION_ERROR
 * This allows the use of a message to be passed back rather than just an error
 * number.
 ******************************************************************************/
BEGIN
DECLARE
  BEGIN
  
    RAISE_APPLICATION_ERROR(-20000, 'A sample error message message');
    
  END;
EXCEPTION
  WHEN OTHERS THEN
    DBMS_OUTPUT.PUT_LINE('Error Code: ' || SQLCODE || ' Message: ' || SQLERRM);
END;
/

