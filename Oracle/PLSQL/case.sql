SET SERVEROUTPUT ON;


/*******************************************************************************
 | There are a few different formats of CASE statements in PL/SQL. 
 |
 | A case statement to evaluate if a value matches potentially many other 
 | values, a case statement that works like an IF / ELSIF / ELSE clause, and
 | a case statement that can be used to return data based on the condition that
 | is matched.
 |
 | One difference from an if statement, if none of the conditions match, an
 | exception will be thrown.
 ******************************************************************************/
 
 
/*******************************************************************************
 | This conditional statement is similar replacement for an IF condition.
 | Each condition will be evaluated, and the block associated with the condition
 | that is evaluated to true will be executed.
 |
 ******************************************************************************/
DECLARE
  cost_of_a_laptop NUMBER;
BEGIN

  cost_of_a_laptop := 500;
  
  CASE
    WHEN cost_of_a_laptop >= 0 AND cost_of_a_laptop < 400 THEN
      DBMS_OUTPUT.PUT_LINE('What''s wrong with it?');
    WHEN cost_of_a_laptop >= 400 AND cost_of_a_laptop < 700 THEN
      DBMS_OUTPUT.PUT_LINE('This may last you a year.');
    WHEN cost_of_a_laptop >= 700 AND cost_of_a_laptop < 1000 THEN
      DBMS_OUTPUT.PUT_LINE('This is a good laptop!');
    WHEN cost_of_a_laptop >= 1000 THEN
      DBMS_OUTPUT.PUT_LINE('WOW! This is going to be one great laptop.');
    ELSE
      DBMS_OUTPUT.PUT_LINE('Nothing matched');
  END CASE;
END;
/


/*******************************************************************************
 | This condition evaluates some value against a set of other values.  If any of
 | the values match, the block associated with the value is executed.
 ******************************************************************************/
DECLARE
  hello VARCHAR(30);
BEGIN
  hello := 'world';
  
  CASE hello
    WHEN 'neighborhood' THEN
      DBMS_OUTPUT.PUT_LINE('You''re a good neighbor!');
    WHEN 'city' THEN
      DBMS_OUTPUT.PUT_LINE('You''re friendly, but think bigger!');
    WHEN 'world' THEN
      DBMS_OUTPUT.PUT_LINE('That''s a very nice greeting.');
    WHEN 'universe' THEN
      DBMS_OUTPUT.PUT_LINE('You''re going a little too far!');
    -- If no value is matched, then the CASE will throw an exception
  END CASE;
END;
/



/*******************************************************************************
 | This condition evalutes to the value of the expression for the given block of
 | code.
 ******************************************************************************/
DECLARE
  hello VARCHAR(30);
  caseResult VARCHAR(1024);
BEGIN
  hello := 'universe';
  
  caseResult :=
    CASE hello
      WHEN 'neighborhood' THEN 'You''re a good neighbor!'
      WHEN 'city' THEN 'You''re friendly, but think bigger!'
      WHEN 'world' THEN 'That''s a very nice greeting.'
      WHEN 'universe' THEN 'You''re going a little too far!'
    END;

  DBMS_OUTPUT.PUT_LINE(caseResult);
END;
/