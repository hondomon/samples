This is a repository for samples from my blog.

devjeremy.wordpress.com

While most samples should be harmless, all the samples in this repository are provided as is with no warranty if you run them. In other words, run them at your own risk.  I am not liable for any actions they may take. 

While I try to maintain the samples to the best of my ability, certain samples may not be complete.  

I hope you find these samples useful, and they aid you in your programming adventures!
